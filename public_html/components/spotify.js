var Spotify = new SpotifyWebApi();
Spotify.getToken().then(function (response) {
  console.log("Carregado")
  Spotify.setAccessToken(response.token);
});
// Spotify.setAccessToken('BQD-p11hAVqADUMM39t_no2bkaoVl6BhRRXdqlFUYLN3XRuZq-ZrGfC0T2CoN376g4wG6FZ4Pe3qEHx3ruijCQ');

var audioEl;
var track;

AFRAME.registerComponent('spotify', {
  init: function () {
    
    var el = this.el;
    annyang.setLanguage('pt-BR');
    // Set up speech recognition.
    annyang.addCommands({
      'tocar *song': this.searchTrack.bind(this)
    });
    annyang.start();

    // Create audio element to point to Spotify preview URL.
    
    audioEl = this.audioEl = document.createElement('audio');

    $("body").append($("<button></button>", {
      'text':'play',
      'id': 'playbutton',
      'style': 'position: absolute;bottom: 0;'
    }));
  },

  searchTrack: function (query) {
    var audioEl = this.audioEl;
    var el = this.el;

    Spotify.searchTracks(query).then(function (results) {
      
      track = results.tracks.items[0];
      var previewUrl = track.preview_url;
      el.emit('spotify-play', results);
      el.emit('scale');
      document.querySelector('#thumb_music').setAttribute('src', track.album.images[0].url)
      
      document.querySelector('#playbutton').addEventListener('click', function() {

    
        audioEl.crossOrigin = 'anonymous';
        audioEl.loop = false;
        audioEl.id = 'spotifyTrack';
        audioEl.src = track.preview_url;
        
        el.appendChild(audioEl);
        el.setAttribute('audioanalyser', {src: '#spotifyTrack'});
        console.log("tocando " + audioEl.src);
        audioEl.play();
        console.log("tocando fim");
      });

      el.on('spotify-play', () => {
        console.log("play")
        audioEl.crossOrigin = 'anonymous';
        audioEl.loop = false;
        audioEl.id = 'spotifyTrack';
        audioEl.src = track.preview_url;
        
        el.appendChild(audioEl);
        el.setAttribute('audioanalyser', {src: '#spotifyTrack'});
        console.log("tocando " + audioEl.src);
        audioEl.play();
      });

      // setTimeout( () => {
      //   $("#playbutton").trigger("click")
      // }, 1000);
    });
  }
});
